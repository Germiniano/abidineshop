import {DatePipe, formatDate} from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { DeliveryService } from '../delivery.service';

@Component({
  selector: 'app-pac',
  templateUrl: './pac.component.html',
  styleUrls: ['./pac.component.css']
})
export class PacComponent implements OnInit {
  method ={
    name:"pac",
    active:true,
    ceporigem:74015010,
    addtime: 0,
    extrafreightvalue: 0,
    addextrafreightvalue: true,
    minvalue:0,
    token:" "

  }
  myDate:any;
  constructor(private deliveryService: DeliveryService) { }

  ngOnInit(): void {
    this.myDate = formatDate(new Date(), 'dd/MM/yyyy', 'en');
    this.deliveryService.getDelivery('pac').subscribe(data => {
      console.log(data);
      this.method=data[1];
      console.log(this.method);
    });
  }
  update(){
    console.log(this.method);
    this.deliveryService.setDelivery(this.method).subscribe(data => {
      console.log(data);
        if(data.result== "update page"){
          alert("pagina atualizada com sucesso");
        }else{
          alert("erro ao inserir a configuração");
        }
    });
  }
}

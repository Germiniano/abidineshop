import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ProductsComponent } from './products/products.component';
import { ProductsformComponent } from './productsform/productsform.component';
import { ProductsserviceComponent } from './productsservice/productsservice.component';
import { ProductscategoryComponent } from './productscategory/productscategory.component';
import { ProductsmassupComponent } from './productsmassup/productsmassup.component';
import { ServicesFormComponent } from './services-form/services-form.component';
import { ProductsCategoryFormComponent } from './products-category-form/products-category-form.component';
import { SettingsComponent } from './settings/settings.component';
import { EcommerceComponent } from './ecommerce/ecommerce.component';
import { ClientMsgComponent } from './client-msg/client-msg.component';
import { ClientRatesComponent } from './client-rates/client-rates.component';
import { ReportsComponent } from './reports/reports.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { ProductForm1Component } from './product-form1/product-form1.component';
import { ProductForm2Component } from './product-form2/product-form2.component';
import { ProductForm3Component } from './product-form3/product-form3.component';
import { ProductForm4Component } from './product-form4/product-form4.component';
import { ProductForm5Component } from './product-form5/product-form5.component';
import { ProductForm6Component } from './product-form6/product-form6.component';
import { CategoryForm1Component } from './category-form1/category-form1.component';
import { CategoriesComponent } from './categories/categories.component';
import {OrdersComponent} from './orders/orders.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { ConfigfaqComponent } from './configfaq/configfaq.component';
import { ConfigvisualntextComponent } from './configvisualntext/configvisualntext.component';
import { ConfigaboutusComponent } from './configaboutus/configaboutus.component';
import { ConfigprivacyComponent } from './configprivacy/configprivacy.component';
import { ConfigdeliverypolComponent } from './configdeliverypol/configdeliverypol.component';
import { DatePipe } from './date.pipe';
import { BRLPipe } from './brl.pipe';
import { NewsletterComponent } from './newsletter/newsletter.component';
import { Editproduct1Component } from './editproduct1/editproduct1.component';
import { Editproduct2Component } from './editproduct2/editproduct2.component';
import { Editproduct3Component } from './editproduct3/editproduct3.component';
import { Editproduct4Component } from './editproduct4/editproduct4.component';
import { Editproduct5Component } from './editproduct5/editproduct5.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ConfigComponent } from './config/config.component';
import { SedexComponent } from './sedex/sedex.component';
import { PacComponent } from './pac/pac.component';
import { FrenetComponent } from './frenet/frenet.component';
import { TransporterComponent } from './transporter/transporter.component';
import { InhandComponent } from './inhand/inhand.component';
import { MelhorenvioComponent } from './melhorenvio/melhorenvio.component';
import { EditcategoryComponent } from './editcategory/editcategory.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { MotoboyComponent } from './motoboy/motoboy.component';
import { DeliveryareaComponent } from './deliveryarea/deliveryarea.component';
import { RegistercompanyComponent } from './registercompany/registercompany.component';
import { RegisteruserComponent } from './registeruser/registeruser.component';
import { HelpcenterComponent } from './helpcenter/helpcenter.component';
import { HelpcenterintComponent } from './helpcenterint/helpcenterint.component';
import { ConfighelpcenterComponent } from './confighelpcenter/confighelpcenter.component';
import { AboutcompanyComponent } from './aboutcompany/aboutcompany.component';
import { OrderdetailComponent } from './orderdetail/orderdetail.component';
import { BulkupComponent } from './bulkup/bulkup.component';
import { CompaniesComponent } from './companies/companies.component';
import { TermsprivacyComponent } from './termsprivacy/termsprivacy.component';
import { Error404Component } from './error404/error404.component';
import { Error401Component } from './error401/error401.component';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ProductsComponent,
    ProductsformComponent,
    ProductsserviceComponent,
    ProductscategoryComponent,
    ProductsmassupComponent,
    ServicesFormComponent,
    ProductsCategoryFormComponent,
    SettingsComponent,
    EcommerceComponent,
    ClientMsgComponent,
    ClientRatesComponent,
    ReportsComponent,
    HeaderComponent,
    FooterComponent,
    ProductForm1Component,
    ProductForm2Component,
    ProductForm3Component,
    ProductForm4Component,
    ProductForm5Component,
    ProductForm6Component,
    OrdersComponent,
    CategoryForm1Component,
    CategoriesComponent,
    ConfigfaqComponent,
    ConfigvisualntextComponent,
    ConfigaboutusComponent,
    ConfigprivacyComponent,
    ConfigdeliverypolComponent,
    DatePipe,
    BRLPipe,
    NewsletterComponent,
    Editproduct1Component,
    Editproduct2Component,
    Editproduct3Component,
    Editproduct4Component,
    Editproduct5Component,
    NavbarComponent,
    ConfigComponent,
    SedexComponent,
    PacComponent,
    FrenetComponent,
    TransporterComponent,
    InhandComponent,
    MelhorenvioComponent,
    EditcategoryComponent,
    MotoboyComponent,
    DeliveryareaComponent,
    RegistercompanyComponent,
    RegisteruserComponent,
    HelpcenterComponent,
    HelpcenterintComponent,
    ConfighelpcenterComponent,
    AboutcompanyComponent,
    OrderdetailComponent,
    BulkupComponent,
    CompaniesComponent,
    TermsprivacyComponent,
    Error404Component,
    Error401Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(),
    FormsModule,
    ReactiveFormsModule, // ToastrModule added,
    Ng2SearchPipeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

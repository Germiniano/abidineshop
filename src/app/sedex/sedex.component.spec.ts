import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SedexComponent } from './sedex.component';

describe('SedexComponent', () => {
  let component: SedexComponent;
  let fixture: ComponentFixture<SedexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SedexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SedexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

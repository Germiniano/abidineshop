import {DatePipe, formatDate} from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { DeliveryService } from '../delivery.service';

@Component({
  selector: 'app-sedex',
  templateUrl: './sedex.component.html',
  styleUrls: ['./sedex.component.css']
})
export class SedexComponent implements OnInit {
  method ={
    name:"sedex",
    active:true,
    ceporigem:74015010,
    addtime: 0,
    extrafreightvalue: 0,
    addextrafreightvalue: true,
    minvalue:0,
    token:" "

  }
  myDate:any;

  constructor(private deliveryService: DeliveryService) { }

  ngOnInit(): void {
    this.myDate = formatDate(new Date(), 'dd/MM/yyyy', 'en');
    this.deliveryService.getDelivery('sedex').subscribe(data=>{
      if(data){
        console.log(data);
        this.method=data;
      }

    });
  }
  update(){
    console.log(this.method);
    this.deliveryService.setDelivery(this.method).subscribe(data=>{
      console.log(data);
    });
  }
}


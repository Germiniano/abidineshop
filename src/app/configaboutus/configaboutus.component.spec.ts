import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigaboutusComponent } from './configaboutus.component';

describe('ConfigaboutusComponent', () => {
  let component: ConfigaboutusComponent;
  let fixture: ComponentFixture<ConfigaboutusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigaboutusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigaboutusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Router } from '@angular/router';
import { Component, OnInit , EventEmitter} from '@angular/core';
import * as ClassicEditorBuild from '@ckeditor/ckeditor5-build-classic';
import { CKEditor5 } from '@ckeditor/ckeditor5-angular/ckeditor';
import { PageService } from '../page.service';
import { DatePipe } from '@angular/common';
import {formatDate} from '@angular/common';



@Component({
  selector: 'app-configaboutus',
  templateUrl: './configaboutus.component.html',
  styleUrls: ['./configaboutus.component.css'],
  providers: [DatePipe]
})
export class ConfigaboutusComponent implements OnInit {
  myDate : any;
  page: any;
  public ClassicEditorBuild = ClassicEditorBuild;
  public editor: CKEditor5.Editor = null;
  public readyEmitter = new EventEmitter<CKEditor5.Editor>();
  constructor( private pageService: PageService,private router: Router ,
               private datePipe: DatePipe){
    this.myDate = formatDate(new Date(), 'dd/MM/yyyy', 'en');
    const month = this.myDate.toLocaleString('default', { month: 'long' });
    console.log(month);


    this.page = {};
  }

  ngOnInit(): void {
  }
  public onReady( editor: CKEditor5.Editor ) {
    this.editor = editor;
    this.readyEmitter.emit( this.editor );
  }

  setMetaTagDescription(event: Event){
    this.page.tagdescription = (<HTMLInputElement> event.target).value;
    console.log(this.page);
    console.log(this.editor.getData());
  }
  setTagTitle (event: Event){
    console.log(this.page);
    this.page.tagtitle = (<HTMLInputElement> event.target).value;
    console.log(this.editor.getData());
  }
  setMetaTagKeywords(event: Event){
    console.log(this.page);
    this.page.tagkeywords = (<HTMLInputElement> event.target).value;
    console.log(this.editor.getData());
  }
  savePage(){
    this.page.ckeditor = this.editor.getData();
    this.pageService.setAboutUsData(this.page).subscribe((data) => {
      console.log(data);
      if(data.result== "update page"){
        alert("Página atualizada com sucesso.");
      }else{
        alert("Erro ao inserir a configuração");
      }
    });
   
  }
  
  
}

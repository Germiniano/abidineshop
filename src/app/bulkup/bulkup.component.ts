import { Component, OnInit } from '@angular/core';
import {DatePipe, formatDate} from '@angular/common';

@Component({
  selector: 'app-bulkup',
  templateUrl: './bulkup.component.html',
  styleUrls: ['./bulkup.component.css']
})
export class BulkupComponent implements OnInit {
  myDate:any;

  constructor() { }

  ngOnInit(): void {
    this.myDate = formatDate(new Date(), 'dd/MM/yyyy', 'en');

  }

}

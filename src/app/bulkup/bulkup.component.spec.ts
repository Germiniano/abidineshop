import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BulkupComponent } from './bulkup.component';

describe('BulkupComponent', () => {
  let component: BulkupComponent;
  let fixture: ComponentFixture<BulkupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BulkupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BulkupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

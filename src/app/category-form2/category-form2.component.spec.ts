import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoryForm2Component } from './category-form2.component';

describe('CategoryForm2Component', () => {
  let component: CategoryForm2Component;
  let fixture: ComponentFixture<CategoryForm2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoryForm2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryForm2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

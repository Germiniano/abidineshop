import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-category-form2',
  templateUrl: './category-form2.component.html',
  styleUrls: ['./category-form2.component.css']
})
export class CategoryForm2Component implements OnInit {

  constructor(
    private userservice: UserService,
    private router: Router) { }

  ngOnInit(): void {
  }

}

import {DatePipe, formatDate} from '@angular/common';

import { Component, OnInit } from '@angular/core';
import { EcommerceService} from 'src/app/ecommerce.service';
import { Router } from '@angular/router';
import { UserService } from '../user.service';
import { ToastrService } from 'ngx-toastr';
import {  ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import * as _ from 'lodash';
import { ProductService } from '../product.service';




@Component({
  selector: 'app-product-form1',
  templateUrl: './product-form1.component.html',
  styleUrls: ['./product-form1.component.css']
})
export class ProductForm1Component implements OnInit {

  name: string = '';
  desc: string = '';
  shortdesc: string = '';
  photourl: any [];
  oldprice: any;
  actualprice: any;
  videourl: any;
  user: any;
  product: any;
  myDate:any;
  title = 'angular-image-file-upload-tutorial';

  @ViewChild('UploadFileInput', { static: false }) uploadFileInput: ElementRef;
  fileUploadForm: FormGroup;
  fileInputLabel: string;


  constructor(  private ec: EcommerceService,
                private router: Router, private toastr: ToastrService,
                private userservice: UserService,
                private ps: ProductService,
                private http: HttpClient,
                private formBuilder: FormBuilder
   ) { }



  ngOnInit(): void {
    this.myDate = formatDate(new Date(), 'dd/MM/yyyy', 'en');

    if (this.userservice.isSigned() === false ){
      this.router.navigateByUrl('/login');
    }else{
      this.user = this.userservice.isSigned();

    }
    this.product = {};
    this.product.photourl =[];
    this.fileUploadForm = this.formBuilder.group({
      uploadedImage: ['']
    });
  }
  onFileSelect(event) {
    const file = event.target.files[0];
    this.fileInputLabel = file.name;
    this.fileUploadForm.get('uploadedImage').setValue(file);
  }
  onFormSubmit() {

    if (!this.fileUploadForm.get('uploadedImage').value) {
      alert('Please fill valid details!');
      return false;
    }

    const formData = new FormData();
    formData.append('uploadedImage', this.fileUploadForm.get('uploadedImage').value);
    formData.append('agentId', '007');


    this.http
      .post<any>('http://localhost:3100/api/fileupload', formData).subscribe(response => {
        console.log(response);
        this.product.photourl.push(response.imgpath);
        if (response.statusCode === 200) {
          // Reset the file input
          this.uploadFileInput.nativeElement.value = "";
          this.fileInputLabel = undefined;

        }
      }, er => {
        console.log(er);
        alert(er.error.error);
      });
  }




addProductName( event: Event){
  this.product.name = (<HTMLInputElement> event.target).value;
  console.log(this.product);

}
addProductDesc( event: Event){
  this.product.fulldescription = (<HTMLInputElement> event.target).value;
  console.log(this.product);
}
addProductShortDesc(event: Event){
  this.product.shortdescription = (<HTMLInputElement> event.target).value;
  console.log(this.product);
}
addProductOldPrice (event: Event){
  this.product.oldprice = parseFloat((<HTMLInputElement> event.target).value);
  console.log(this.product);
}
addProductActualPrice (event: Event){
  this.product.price = parseFloat((<HTMLInputElement> event.target).value);
  console.log(this.product);
}
addProduct (){
  console.log(this.product);
}
addProductVideoURL (event: Event){
  this.product.videourl = (<HTMLInputElement> event.target).value;
  console.log(this.product);
}
addProductMostSold(event: Event){
  this.product.mostSold = true;
  console.log(this.product);
}
addProductSecondwDisc(event: Event){
  this.product.secondwDisc = true;
  console.log(this.product);
}
addProductP2l3(event: Event){
  this.product.p2l3 = true;
  console.log(this.product);
}
addProductImgPath (event: Event){
  this.product.photourl = (<HTMLInputElement> event.target).value;
  console.log(this.product);
}
nextStep(){

  if(this.validateForm()!=''){ alert(this.validateForm())};
  this.ec.addProduct(this.product).subscribe( data =>{
    console.log('product',data);
    let product ;

     product  =data;
    this.ps.set(product.data);

  this.router.navigateByUrl('/produtosform2');

  });

}

validateForm(){
  if(!this.product.name) return 'Digite o nome do produto';
  if(!this.product.fulldescription) return 'Digite a descrição completa do produto';
  if(!this.product.price) return 'Digite o preço atual';

   return'';

  }


}





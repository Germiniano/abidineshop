import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductForm1Component } from './product-form1.component';

describe('ProductForm1Component', () => {
  let component: ProductForm1Component;
  let fixture: ComponentFixture<ProductForm1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductForm1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductForm1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

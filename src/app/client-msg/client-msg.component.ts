
import {DatePipe, formatDate} from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { EcommerceService} from 'src/app/ecommerce.service';
import { Router } from '@angular/router';
import { UserService } from '../user.service';
@Component({
  selector: 'app-client-msg',
  templateUrl: './client-msg.component.html',
  styleUrls: ['./client-msg.component.css']
})
export class ClientMsgComponent implements OnInit {
msgs: any;
user: any;
myDate:any;
  constructor(private ec: EcommerceService,
              private router: Router,
              private userservice: UserService) { }

  ngOnInit(): void {
    this.myDate = formatDate(new Date(), 'dd/MM/yyyy', 'en');
    if (this.userservice.isSigned() === false ){
      this.router.navigateByUrl('/login');
  }else{
      this.user = this.userservice.isSigned();
  }
    this.ec.getAllClientMsg().subscribe((data) => {
      this.msgs = data;

    });
  }

}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import {ProductsComponent} from './products/products.component'
import { ProductForm1Component } from './product-form1/product-form1.component';
import { ProductForm2Component } from './product-form2/product-form2.component';
import { ProductForm3Component } from './product-form3/product-form3.component';
import { ProductForm4Component } from './product-form4/product-form4.component';
import { ProductForm5Component } from './product-form5/product-form5.component';
import { ProductForm6Component } from './product-form6/product-form6.component';
import { CategoryForm1Component } from './category-form1/category-form1.component';
import { CategoryForm2Component } from './category-form2/category-form2.component';
import {CategoriesComponent} from './categories/categories.component'
import { ServiceForm1Component } from './service-form1/service-form1.component';
import { ServiceForm2Component } from './service-form2/service-form2.component';
import { ServiceForm3Component } from './service-form3/service-form3.component';
import { ClientMsgComponent } from './client-msg/client-msg.component';
import { ClientRatesComponent } from './client-rates/client-rates.component';
import { Register1Component} from './register1/register1.component';
import { Register2Component} from './register2/register2.component';
import { Register3Component} from './register3/register3.component';
import { Register4Component} from './register4/register4.component';
import { LoginComponent} from './login/login.component';
import {OrdersComponent} from './orders/orders.component';
import {EcommerceComponent} from './ecommerce/ecommerce.component';
import {ConfigfaqComponent} from './configfaq/configfaq.component';
import {ConfigaboutusComponent} from './configaboutus/configaboutus.component';
import {ConfigprivacyComponent} from './configprivacy/configprivacy.component';
import {ConfigdeliverypolComponent} from './configdeliverypol/configdeliverypol.component';
import {ConfigvisualntextComponent} from './configvisualntext/configvisualntext.component';
import {NewsletterComponent } from './newsletter/newsletter.component';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { Editproduct1Component } from './editproduct1/editproduct1.component';
import { Editproduct2Component } from './editproduct2/editproduct2.component';
import { Editproduct3Component } from './editproduct3/editproduct3.component';
import { Editproduct4Component } from './editproduct4/editproduct4.component';
import { Editproduct5Component } from './editproduct5/editproduct5.component';
import { ConfigComponent } from './config/config.component';
import {SedexComponent} from "./sedex/sedex.component";
import { FrenetComponent } from "./frenet/frenet.component";
import {PacComponent} from "./pac/pac.component";
import {MelhorenvioComponent} from "./melhorenvio/melhorenvio.component";
import {InhandComponent} from "./inhand/inhand.component";
import {TransporterComponent} from "./transporter/transporter.component";
import {DeliveryareaComponent} from "./deliveryarea/deliveryarea.component";
import {RegistercompanyComponent} from "./registercompany/registercompany.component";
import {RegisteruserComponent} from "./registeruser/registeruser.component";
import {HelpcenterComponent} from "./helpcenter/helpcenter.component";
import {HelpcenterintComponent} from "./helpcenterint/helpcenterint.component";
import {ConfighelpcenterComponent} from "./confighelpcenter/confighelpcenter.component";
import {AboutcompanyComponent} from "./aboutcompany/aboutcompany.component";
import {OrderdetailComponent} from "./orderdetail/orderdetail.component";
import {BulkupComponent} from "./bulkup/bulkup.component";
import {CompaniesComponent} from "./companies/companies.component";
import {TermsprivacyComponent} from "./termsprivacy/termsprivacy.component";
import {AboutusComponent} from "./aboutus/aboutus.component";
import {PrivacyComponent} from './privacy/privacy.component';
import {Error401Component} from './error401/error401.component';
import {Error404Component} from './error404/error404.component';




import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
const routes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'home', component: HomeComponent},
  { path: 'anuncios', component: ProductsComponent },
  { path: 'produtosform1', component: ProductForm1Component },
  { path: 'produtosform2', component: ProductForm2Component },
  { path: 'produtosform3', component: ProductForm3Component },
  { path: 'produtosform4', component: ProductForm4Component },
  { path: 'produtosform5', component: ProductForm5Component},
  { path: 'produtosform6', component: ProductForm6Component },
  { path: 'categorias', component: CategoriesComponent },
  { path: 'categoriasform1', component: CategoryForm1Component },
  { path: 'categoriasform2', component: CategoryForm2Component },
  { path: 'categorias', component: CategoryForm2Component },
  { path: 'servicosform1', component: ServiceForm1Component },
  { path: 'servicosform2', component: ServiceForm2Component },
  { path: 'servicosform3', component: ServiceForm3Component  },
  { path: 'clientemsg', component: ClientMsgComponent },
  { path: 'clienteavalia', component: ClientRatesComponent },
  { path: 'pedidos', component: OrdersComponent},
  { path: 'registro1', component: Register1Component },
  { path: 'registro2', component: Register2Component },
  { path: 'registro3', component: Register3Component },
  { path: 'registro4', component: Register4Component },
  { path: 'login', component: LoginComponent },
  { path: 'faq', component: ConfigfaqComponent },
  { path: 'configsobrenos', component: ConfigaboutusComponent },
  { path: 'configprivacidade', component: ConfigprivacyComponent},
  { path: 'politicaentrega', component: ConfigdeliverypolComponent },
  { path: 'visualetexto', component: ConfigvisualntextComponent },
  { path: 'ecommerce', component: EcommerceComponent },
  { path: 'exportar-receba-novidades', component: NewsletterComponent },
  { path: 'editarprodutosform1/:pid', component: Editproduct1Component },
  { path: 'editarprodutosform2/:product', component: Editproduct2Component },
  { path: 'editarprodutosform3/:product', component: Editproduct3Component },
  { path: 'editarprodutosform4/:product', component: Editproduct4Component },
  { path: 'editarprodutosform5/:product', component: Editproduct5Component},
  {path: 'configuracoes', component: ConfigComponent },
  {path: 'transportadora',component: TransporterComponent},
  {path: 'pac', component: PacComponent},
  {path: 'sedex', component: SedexComponent},
  {path: 'frenet', component: FrenetComponent},
  {path: 'melhorenvio', component: MelhorenvioComponent},
  {path: 'retirada', component: InhandComponent},
  {path: 'areadeentrega', component: DeliveryareaComponent},
  {path: 'cadastrarempresa', component: RegistercompanyComponent},
  {path: 'cadastrarusuario', component: RegisteruserComponent},
  {path: 'centraldeajuda', component: HelpcenterComponent},
  {path: 'centraldeajudainterna', component: HelpcenterintComponent},
  {path: 'configcentraldeajuda', component: ConfighelpcenterComponent},
  {path: 'sobreaempresa', component: AboutcompanyComponent},
  {path: 'pedidosdetalhes', component: OrderdetailComponent},
  {path: 'uploademmassa', component: BulkupComponent},
  {path: 'todasempresas', component: CompaniesComponent},
  {path: 'termoseprivacidade', component: TermsprivacyComponent},
  {path: 'sobrenos', component: AboutusComponent },
  {path: 'privacidade', component: PrivacyComponent},
  {path: 'error401', component: Error401Component},
  {path: 'error404', component: Error404Component}


  

];

@NgModule({
  imports: [RouterModule.forRoot(routes),
            CommonModule,
            BrowserModule,
            CKEditorModule
           ],
  exports: [RouterModule, CKEditorModule]
})
export class AppRoutingModule { }

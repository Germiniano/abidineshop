import {DatePipe, formatDate} from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {ActivatedRoute} from '@angular/router';
import { EcommerceService} from 'src/app/ecommerce.service';
import { UserService } from '../user.service';
import { ProductService } from '../product.service';
@Component({
  selector: 'app-product-form2',
  templateUrl: './product-form2.component.html',
  styleUrls: ['./product-form2.component.css']
})
export class ProductForm2Component implements OnInit {



  myDate:any;
  product: any;
  categories: any;
  categoriesAdded: any;
  user: any;
  constructor(private router: Router,
              public activatedRoute: ActivatedRoute,
              private ec: EcommerceService,
              private userService: UserService,
              private ps: ProductService
   ) { }



  ngOnInit(): void {
    this.myDate = formatDate(new Date(), 'dd/MM/yyyy', 'en');
    if (this.userService.isSigned() === false ){
        this.router.navigateByUrl('/login');
    }else{
        this.user = this.userService.isSigned();
    }
    this.product=this.ps.get();
    console.log(this.product);
    this.product.categories=[];
    console.log(this.product);

    this.ec.getAllCategories().subscribe((data) => {
      this.categories = data;
      console.log(this.categories);
    });
  }



addCategory(id,category){
  this.product.categories.push(category);
  console.log(this.product.categories);
}
removeCategory(e: Event ){

}

nextStep(){
  console.log(this.product);
  let productString = JSON.stringify(this.product);
  this.ps.set(this.product);
  this.product.quantity = this.product.price;
    this.ec.updateProduct(this.product)
      .subscribe(data=>{
        let product ;
         product  =data;
        this.ps.set(product.data);
        this.router.navigateByUrl('/produtosform3');
      });

}


}

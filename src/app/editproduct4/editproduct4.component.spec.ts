import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Editproduct4Component } from './editproduct4.component';

describe('Editproduct4Component', () => {
  let component: Editproduct4Component;
  let fixture: ComponentFixture<Editproduct4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Editproduct4Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Editproduct4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { EcommerceService} from 'src/app/ecommerce.service';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../user.service';
@Component({
  selector: 'app-editproduct4',
  templateUrl: './editproduct4.component.html',
  styleUrls: ['./editproduct4.component.css']
})
export class Editproduct4Component implements OnInit {
  myDate:any;
  user: any;
  product: any;
  constructor(private router: Router,
              public activatedRoute: ActivatedRoute,
              private userservice: UserService
) { }



ngOnInit(): void {
  if (this.userservice.isSigned() === false ){
    this.router.navigateByUrl('/login');
  }else{
      this.user = this.userservice.isSigned();
  }
  this.activatedRoute.paramMap.subscribe((paramMap) => {
    this.product = JSON.parse(paramMap.get('product'));
    console.log('Product ID found: ', this.product);
  });
}

addProductNCM(event: Event){
  this.product.ncm=(<HTMLInputElement> event.target).value;
}
addProductGtin(event: Event){
  this.product.gtin=(<HTMLInputElement> event.target).value;
}


  nextStep(){
    let productString = JSON.stringify(this.product);
    this.router.navigateByUrl('/editarprodutosform5/'+ productString);
  }


}

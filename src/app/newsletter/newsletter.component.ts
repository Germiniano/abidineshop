import {DatePipe, formatDate} from '@angular/common';
import { Component, OnInit } from '@angular/core';
import {PageService} from '../page.service';

@Component({
  selector: 'app-newsletter',
  templateUrl: './newsletter.component.html',
  styleUrls: ['./newsletter.component.css']
})
export class NewsletterComponent implements OnInit {
  excelFile:any;
  myDate:any;
  constructor( private pageService: PageService) { }

  ngOnInit(): void {
  this.myDate = formatDate(new Date(), 'dd/MM/yyyy', 'en');
  }
  getExcel(){
    this.excelFile = this.pageService.getExcelNewsletter();
  }

}

import {DatePipe, formatDate} from '@angular/common';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-service-form2',
  templateUrl: './service-form2.component.html',
  styleUrls: ['./service-form2.component.css']
})
export class ServiceForm2Component implements OnInit {
  myDate:any
  constructor() { }

  ngOnInit(): void {
    this.myDate = formatDate(new Date(), 'dd/MM/yyyy', 'en');
  }

}

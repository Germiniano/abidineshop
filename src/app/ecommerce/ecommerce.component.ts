import {DatePipe, formatDate} from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ecommerce',
  templateUrl: './ecommerce.component.html',
  styleUrls: ['./ecommerce.component.css']
})
export class EcommerceComponent implements OnInit {

  myDate:any;
  constructor( private userservice: UserService,
               private router: Router) { }

  ngOnInit(): void {
    this.myDate = formatDate(new Date(), 'dd/MM/yyyy', 'en');
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceForm1Component } from './service-form1.component';

describe('ServiceForm1Component', () => {
  let component: ServiceForm1Component;
  let fixture: ComponentFixture<ServiceForm1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServiceForm1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceForm1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

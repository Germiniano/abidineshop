import {DatePipe, formatDate} from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { EcommerceService} from 'src/app/ecommerce.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-service-form1',
  templateUrl: './service-form1.component.html',
  styleUrls: ['./service-form1.component.css']
})
export class ServiceForm1Component implements OnInit {
  service: any;
  myDate:any;
  constructor(private ec: EcommerceService, private router: Router) { }

  ngOnInit(): void {
    this.myDate = formatDate(new Date(), 'dd/MM/yyyy', 'en');
  }

  nextStep(){

    let serviceString = JSON.stringify(this.service);
    this.router.navigateByUrl('/produtosform2/'+serviceString);
  }
}

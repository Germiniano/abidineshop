import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { UserService} from '../user.service';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  user:any;
  myDate:any
  constructor( private userService: UserService) { }

  ngOnInit(): void {
   this.user = this.userService.isSigned();
  }

}

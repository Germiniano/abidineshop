import { Component, OnInit } from '@angular/core';
import {DatePipe, formatDate} from '@angular/common';
import { Router } from '@angular/router';
import { UserService } from '../user.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [DatePipe]
})
export class HomeComponent implements OnInit {
  user: any;
  myDate:any;
  constructor(private userservice: UserService,
              private router: Router,
              private datePipe: DatePipe) {
    this.myDate = formatDate(new Date(), 'dd/MM/yyyy', 'en');
    const month = this.myDate.toLocaleString('default', { month: 'long' });
    console.log(month);


  }

  ngOnInit(): void {
    this.myDate = formatDate(new Date(), 'dd/MM/yyyy', 'en');
    if (this.userservice.isSigned() === false ){
      this.router.navigateByUrl('/login');
    }else{
      this.user = this.userservice.isSigned();
    }
  }

}

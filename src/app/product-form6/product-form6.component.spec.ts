import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductForm6Component } from './product-form6.component';

describe('ProductForm6Component', () => {
  let component: ProductForm6Component;
  let fixture: ComponentFixture<ProductForm6Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductForm6Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductForm6Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {DatePipe, formatDate} from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { EcommerceService} from 'src/app/ecommerce.service';
import { Router } from '@angular/router';
import { UserService } from '../user.service';
@Component({
  selector: 'app-product-form6',
  templateUrl: './product-form6.component.html',
  styleUrls: ['./product-form6.component.css']
})
export class ProductForm6Component implements OnInit {

  myDate:any;
  constructor(private router: Router,
              private userservice: UserService) { }

  ngOnInit(): void {
    this.myDate = formatDate(new Date(), 'dd/MM/yyyy', 'en');
  }
  nextStep(){
    this.router.navigateByUrl('/produtosform1');
  }


}

import {DatePipe, formatDate} from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { EcommerceService} from 'src/app/ecommerce.service';
import { BrowserModule } from '@angular/platform-browser';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {
  orders: any;
  user: any;
  myDate:any;
  constructor(private ec: EcommerceService,
              private userservice: UserService,
              private router: Router) { }

  ngOnInit(): void {
    this.myDate = formatDate(new Date(), 'dd/MM/yyyy', 'en');
    this.ec.getAllOrders().subscribe((data) => {
      this.orders = data;
      console.log(this.orders);
    });
  }

}

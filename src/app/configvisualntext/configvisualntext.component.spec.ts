import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigvisualntextComponent } from './configvisualntext.component';

describe('ConfigvisualntextComponent', () => {
  let component: ConfigvisualntextComponent;
  let fixture: ComponentFixture<ConfigvisualntextComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigvisualntextComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigvisualntextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

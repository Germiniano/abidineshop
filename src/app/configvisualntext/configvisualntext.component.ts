import {DatePipe, formatDate} from '@angular/common';
import {Component, OnInit, EventEmitter, ViewChild, ElementRef} from '@angular/core';
import * as ClassicEditorBuild from '@ckeditor/ckeditor5-build-classic';
import { CKEditor5 } from '@ckeditor/ckeditor5-angular/ckeditor';
import { PageService } from '../page.service';
import {HttpClient} from "@angular/common/http";
import {FormBuilder, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-configvisualntext',
  templateUrl: './configvisualntext.component.html',
  styleUrls: ['./configvisualntext.component.css']
})
export class ConfigvisualntextComponent implements OnInit {


  page: any;
  myDate:any;
  public ClassicEditorBuild = ClassicEditorBuild;
  public editor: CKEditor5.Editor = null;
  public readyEmitter = new EventEmitter<CKEditor5.Editor>();
  @ViewChild('UploadFileInput', { static: false }) uploadFileInput: ElementRef;
  fileUploadForm: FormGroup;
  fileInputLabel: string;

  constructor( private pageService: PageService,
               private http: HttpClient,
               private formBuilder: FormBuilder ) {


    this.page = {};
  }

  ngOnInit(): void {
    this.myDate = formatDate(new Date(), 'dd/MM/yyyy', 'en');
    this.fileUploadForm = this.formBuilder.group({
      uploadedImage: ['']
    });
  }
  public onReady( editor: CKEditor5.Editor ) {
    this.editor = editor;
    this.readyEmitter.emit( this.editor );
    this.fileUploadForm = this.formBuilder.group({
      uploadedImage: ['']
    });
  }
  onFileSelect(event) {
    const file = event.target.files[0];
    this.fileInputLabel = file.name;
    this.fileUploadForm.get('uploadedImage').setValue(file);
  }
  onFormSubmit() {

    if (!this.fileUploadForm.get('uploadedImage').value) {
      alert('Please fill valid details!');
      return false;
    }

    const formData = new FormData();
    formData.append('uploadedImage', this.fileUploadForm.get('uploadedImage').value);
    formData.append('agentId', '007');


    this.http
      .post<any>('http://localhost:3100/api/fileupload', formData).subscribe(response => {
      console.log(response);
      if (response.statusCode === 200) {
        // Reset the file input
        this.uploadFileInput.nativeElement.value = "";
        this.fileInputLabel = undefined;
      }
    }, er => {
      console.log(er);
      alert(er.error.error);
    });
  }




  setMetaTagDescription(event: Event){
    this.page.tagdescription = ( event.target as HTMLInputElement ).value;
    console.log(this.page);
    console.log(this.editor.getData());
  }
  setTagTitle (event: Event){
    console.log(this.page);
    this.page.tagtitle = (<HTMLInputElement> event.target).value;
    console.log(this.editor.getData());
  }
  setMetaTagKeywords(event: Event){
    console.log(this.page);
    this.page.tagkeywords = (<HTMLInputElement> event.target).value;
    console.log(this.editor.getData());
  }
  setInstagram(event: Event){
    console.log(this.page);
    this.page.instagram = (<HTMLInputElement> event.target).value;
    console.log(this.editor.getData());
  }
  setFacebook(event: Event){
    console.log(this.page);
    this.page.facebook = (<HTMLInputElement> event.target).value;
    console.log(this.editor.getData());
  }
  setYoutube(event: Event){
    console.log(this.page);
    this.page.tagkeywords = (<HTMLInputElement> event.target).value;
    console.log(this.editor.getData());
  }
  setPhone1(event: Event){
    console.log(this.page);
    this.page.telephone[0] = (<HTMLInputElement> event.target).value;
    console.log(this.editor.getData());
  }
  setPhone2(event: Event){
    console.log(this.page);
    this.page.telephone[1] = (<HTMLInputElement> event.target).value;
    console.log(this.editor.getData());
  }
  setCompanyAdress(event: Event){
    console.log(this.page);
    this.page.address = (<HTMLInputElement> event.target).value;
    console.log(this.editor.getData());
  }


  setBanner(bannerId, banner){
    console.log(this.page);
    this.page.banner[bannerId].imgurl =null;
    this.page.banner[bannerId].link = (<HTMLInputElement> event.target).value;

  }
  savePage(){
    this.page.ckeditor = this.editor.getData();
    this.pageService.setPrivacyData(this.page).subscribe((data) => {
      console.log(data);
    });
  }
}

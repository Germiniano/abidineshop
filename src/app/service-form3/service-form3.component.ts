import {DatePipe, formatDate} from '@angular/common';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-service-form3',
  templateUrl: './service-form3.component.html',
  styleUrls: ['./service-form3.component.css']
})
export class ServiceForm3Component implements OnInit {
  myDate:any
  constructor() { }

  ngOnInit(): void {
    this.myDate = formatDate(new Date(), 'dd/MM/yyyy', 'en');
  }

}

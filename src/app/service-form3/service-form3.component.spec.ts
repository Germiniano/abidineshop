import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceForm3Component } from './service-form3.component';

describe('ServiceForm3Component', () => {
  let component: ServiceForm3Component;
  let fixture: ComponentFixture<ServiceForm3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServiceForm3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceForm3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

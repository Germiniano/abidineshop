import {DatePipe, formatDate} from '@angular/common';
import { Component, OnInit } from '@angular/core';
import {EcommerceService} from '../ecommerce.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../user.service';
import { ProductService } from '../product.service';
@Component({
  selector: 'app-product-form5',
  templateUrl: './product-form5.component.html',
  styleUrls: ['./product-form5.component.css']
})
export class ProductForm5Component implements OnInit {



  pageTitle = '';
  pageDesc = '';
  pageKeywords: any;
  product: any;
  user: any;
  myDate:any;
  constructor(private activatedRoute: ActivatedRoute,
              private ec: EcommerceService,
              private userservice: UserService,
              private router: Router,
              private ps: ProductService
   ) { }



  ngOnInit(): void {
    this.myDate = formatDate(new Date(), 'dd/MM/yyyy', 'en');
    if (this.userservice.isSigned() === false ){
      this.router.navigateByUrl('/login');
  }else{
      this.user = this.userservice.isSigned();
  }
  this.product=this.ps.get();
  console.log(this.product);
  }



addPageTitle( event: Event){
  this.product.productpagetitle= (<HTMLInputElement> event.target).value;
  console.log(this.product);
}

addPageDesc( event: Event){
  this.product.productpagedesc= (<HTMLInputElement> event.target).value;
  console.log(this.product);
}

addPageKeywords(event: Event){
  this.product.productpagekeywords = (<HTMLInputElement> event.target).value;
  console.log(this.product);
}

addProduct(){
    this.ec.updateProduct(this.product)
      .subscribe(data=>{
        let product ;
        product  =data;
        this.ps.set(product.data);
        this.router.navigateByUrl("/productform6")
      });

}



}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductForm5Component } from './product-form5.component';

describe('ProductForm5Component', () => {
  let component: ProductForm5Component;
  let fixture: ComponentFixture<ProductForm5Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductForm5Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductForm5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {DatePipe, formatDate} from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { DeliveryService } from '../delivery.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-melhorenvio',
  templateUrl: './melhorenvio.component.html',
  styleUrls: ['./melhorenvio.component.css']
})
export class MelhorenvioComponent implements OnInit {
  method ={
    name:"melhorenvio",
    active:true,
    ceporigem:74015010,
    addtime: 0,
    extrafreightvalue: 0,
    addextrafreightvalue: true,
    minvalue:0,
    token:" "

  }
  myDate:any;
  constructor(private deliveryService: DeliveryService,
              private router: Router) { }

  ngOnInit(): void {
    this.myDate = formatDate(new Date(), 'dd/MM/yyyy', 'en');
  }
  update(){
    console.log(this.method);
    this.deliveryService.setDelivery(this.method).subscribe(data=>{
      if(data.result== "update page"){
        alert("pagina atualizada com sucesso");
      }else{
        alert("erro ao inserir a configuração");
      }
    });
  }
}


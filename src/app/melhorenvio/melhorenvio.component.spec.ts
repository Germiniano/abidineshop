import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MelhorenvioComponent } from './melhorenvio.component';

describe('MelhorenvioComponent', () => {
  let component: MelhorenvioComponent;
  let fixture: ComponentFixture<MelhorenvioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MelhorenvioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MelhorenvioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

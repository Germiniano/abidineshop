import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrenetComponent } from './frenet.component';

describe('FrenetComponent', () => {
  let component: FrenetComponent;
  let fixture: ComponentFixture<FrenetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrenetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrenetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PageService {


  page: any;
  apiURL = 'https://stormdeveloper.com.br:3100/api/pages'; // 'https://stormdeveloper.com.br:3100/api/pages';


  constructor(private http: HttpClient) {   }

  setFAQData( faq){
    const url = this.apiURL + '/faq';
    console.log('API URL for all store products: ', this.apiURL);
    this.page = this.http.post(url, faq);
    return  this.page;
  }
  setPrivacyData(privacy){
    const url = this.apiURL + '/privacy';
    console.log(privacy);
    this.page = this.http.put(url, privacy);
    return this.page;
  }
  setDeliveryData(delivery){
    const url = this.apiURL + '/delivery';
    console.log('API URL for all store products: ', this.apiURL);
    this.page = this.http.put('https://stormdeveloper.com.br:3100/api/pages/delivery', delivery);
    return this.page;
  }
  setAboutUsData(aboutus){
    const url = this.apiURL + '/aboutus';
    console.log('API URL for all store products: ', this.apiURL);
    this.page = this.http.put(url, aboutus);
    return this.page;
  }
  setHomeData(home){
    const url = this.apiURL + '/aboutus';
    console.log('API URL for all store products: ', this.apiURL);
    this.page = this.http.put(url, home);
    return this.page;
  }

  getFAQData( ){
    const url = this.apiURL + '/faq';
    console.log('API URL for all store products: ', this.apiURL);
    this.page = this.http.get(url);
    return  this.page;
  }
  getPrivacyData(){
    const url = this.apiURL + '/privacy';

    this.page = this.http.get(url);
    return this.page;
  }
  getDeliveryData(){
    const url = this.apiURL + '/delivery';
    this.page = this.http.get(url);
    return this.page;
  }
  getAboutUsData(){
    const url = this.apiURL + '/aboutus';
    this.page = this.http.get(url);
    return this.page;
  }
  getHomeData(){
    const url = this.apiURL + '/aboutus';
    console.log('API URL for all store products: ', this.apiURL);
    this.page = this.http.get(url);
    return this.page;
  }
  getExcelNewsletter(){
    const url = 'https://stormdeveloper.com.br:3100/api/newsletter/excel';
    console.log('API URL for all store products: ', url);
    const httpOptions = {
      responseType: 'arraybuffer' as 'blob',
      headers: new HttpHeaders({
        'Content-Type': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
      })
    };
    const excel = this.http.get(url, httpOptions).
    subscribe(response => this.downLoadFile(response, 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') );
    return excel;

  }
downLoadFile(data: any, type: string) {
    const blob = new Blob([data], { type});
    const url = window.URL.createObjectURL(blob);
    const pwa = window.open(url);
    if (!pwa || pwa.closed || typeof pwa.closed == 'undefined') {
      alert( 'Please disable your Pop-up blocker and try again.');
    }
  }


}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoryForm1Component } from './category-form1.component';

describe('CategoryForm1Component', () => {
  let component: CategoryForm1Component;
  let fixture: ComponentFixture<CategoryForm1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoryForm1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryForm1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

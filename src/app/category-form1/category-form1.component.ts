import {DatePipe, formatDate} from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { EcommerceService} from 'src/app/ecommerce.service';
import { Router } from '@angular/router';
import { UserService } from '../user.service';
@Component({
  selector: 'app-category-form1',
  templateUrl: './category-form1.component.html',
  styleUrls: ['./category-form1.component.css']
})
export class CategoryForm1Component implements OnInit {
  category: any;
  categories: any;
  user: any;
  id: 0;
  term:any;
  myDate:any;
  constructor(private ec: EcommerceService ,
              private userservice: UserService,
              private router: Router) { }

  ngOnInit(): void {
    this.myDate = formatDate(new Date(), 'dd/MM/yyyy', 'en');
    this.category = {};
    this.ec.getAllCategories().subscribe((data) => {
      this.categories = data;
      this.id = data.length;
      console.log(this.id);
      console.log('All Products from Store: ', this.categories);
    });

  }
  addCategorytName( event: Event){
    this.category.name = (<HTMLInputElement> event.target).value;
    console.log(this.category);
  }
  addCategoryNameGoogle( event: Event){
    this.category.namegoogle = (<HTMLInputElement> event.target).value;
    console.log(this.category);
  }
  addCategoryPageTitle(event: Event){
    this.category.pagetitle= (<HTMLInputElement> event.target).value;
    console.log(this.category);
  }
  addCategoryPageDesc(event: Event){
    this.category.pagedesc = (<HTMLInputElement> event.target).value;
    console.log(this.category);
  }
  addCategoryUrl (event: Event){
    this.category.url = (<HTMLInputElement> event.target).value;
    console.log(this.category);
  }
 addCategoryPageKeywords(event: Event){
    this.category.pagekeywords = (<HTMLInputElement> event.target).value;
    console.log(this.category);
  }
  addCategoryPageMetadata(event: Event){
    this.category.pagemetadata = (<HTMLInputElement> event.target).value;
    console.log(this.category);
  }
  addIconPath (event: Event){
    this.category.iconpath= (<HTMLInputElement> event.target).value;
    console.log(this.category);
  }
  addCategoryTree(event: Event){
    this.category.mother= (<HTMLInputElement> event.target).value;
    console.log(this.category);
  }
  addCategoryfather(event: Event){
    this.category.mother= (<HTMLInputElement> event.target).value;
    console.log(this.category);
  }
  setTerm(event: Event){
    this.term =(<HTMLInputElement> event.target).value;
    console.log(this.term);
  }
  saveCategory(){
  if(this.validateForm()){ alert(this.validateForm())};
  let result;
  this.category.id=this.id+1 ;
  this.ec.addCategory(this.category)
    .then((data) => {
      result =  data;
      console.log(result);
      
     
    })
    .catch(err => {
      console.log(err);
      alert("Erro ao inserir Categoria");
     
      }
      
      
    );
    
  }
  validateForm(){
    if(!this.category.name) return 'Digite o Nome da Categoria';
    if(!this.category.pagetitle) return 'Digite o Título da Página';
    if(!this.category.pagedesc) return 'Digite a Descrição da Página';
    
      return 'Categoria adicionada com sucesso.'; 
      
    }
}

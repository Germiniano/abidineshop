import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class EcommerceService {

  orderResp: any;
  products: any;
    product: any;
    categories: any;
    tags: any;
    apiURL = 'https://stormdeveloper.com.br:3100/api';
    consumerKey = 'ck_63e89ba4a2bc6a5f4bdb9c984f150c5350bfb605';
    consumerSecret = 'cs_3c0fb48a388b6e8a983eaa3f43ad9acaf32d1903';

  constructor(private http: HttpClient) {   }

  getAllStoreProducts(){
    const url = this.apiURL + '/products';
    console.log('API URL for all store products: ', this.apiURL);
    this.products = this.http.get(url);
    return this.products;
  }

//"http://ec2-13-59-143-149.us-east-2.compute.amazonaws.com:3000/api/products"
  getSingleProductById(pId){
   const url = this.apiURL + '/product/' + pId;
   console.log('API URL for single product: ', this.apiURL);
   this.product = this.http.get(url);
   return this.product;
  }
   getSingleProductByName(productName){
   const url = this.apiURL + '/product/' + productName;
   console.log('API URL for single product: ', this.apiURL);
   this.product = this.http.get(url);
   return this.product;
  }

  getProductsByCategory(catId){
    const url = this.apiURL + '/productsbycategory/' + catId;
    console.log('API URL for all store products: ', this.apiURL);
    this.products = this.http.get(url);
    return this.products;
  }


    addProduct(product){
      const url = 'http://localhost:3100/api/product/create';

      console.log('foi ', url);
      const data = this.http.post(url , product);
      return data;
    }



    updateProduct(product){
      const url = 'http://localhost:3100/api/product/update';
      let httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      }
    console.log('foi ', url);
    const data = this.http.put(url , product);
    return  data;
  }

    deleteProduct(product){
    const url = 'http://stormdeveloper.com.br:3100/api/product/create';
    console.log('API URL for all store products: ', this.apiURL);
    this.http.post(url , url );
    return this.products;
  }
  getAllCategories(){
    const url = this.apiURL + '/categories';
    console.log('API URL for all categories: ', this.apiURL);
    this.categories = this.http.get(url);
    return this.categories;
  }
  addCategory(category){
    let headers = new HttpHeaders ({
      'Content-Type': 'application/json'
    });
    const url = this.apiURL + '/category/create';
    console.log('API URL for all store products: ', this.apiURL);
// this.JSON_to_URLEncoded(category)

    return new Promise ((resolve) => {
      this.orderResp = this.http.post(url, category, {headers});
      this.orderResp.subscribe((responseData) => {
        resolve(responseData);
      });
    });
  }
    deleteCategory(product){
    const url = this.apiURL + '/products/create';
    console.log('API URL for all store products: ', this.apiURL);
    return this.products;
  }
  getAllService(){
    const url = this.apiURL + '/categories';
    console.log('API URL for all categories: ', this.apiURL);
    this.categories = this.http.get(url);
    return this.categories;
  }
  addService(category){
    let headers = new HttpHeaders ({
      'Content-Type': 'application/json'
    });
    const url = this.apiURL + '/category/create';
    console.log('API URL for all store products: ', this.apiURL);
// this.JSON_to_URLEncoded(category)

    return new Promise ((resolve) => {
      this.orderResp = this.http.post(url, category, {headers});
      this.orderResp.subscribe((responseData) => {
        resolve(responseData);
      });
    });
  }
    deleteService(product){
    const url = this.apiURL + '/products/create';
    console.log('API URL for all store products: ', this.apiURL);
    return this.products;
  }

  getAllOrders(){
    const url = 'http://localhost:3100/api/orders';
    console.log('API URL for all categories: ', this.apiURL);
    this.categories = this.http.get(url);
    return this.categories;
  }


  getAllClientMsg(){
    const url = this.apiURL + '/clientmsg';
    console.log('API URL for all categories: ', this.apiURL);
    this.categories = this.http.get(url);
    return this.categories;
  }

  getAllClientRatings(){
    const url = this.apiURL + '/clientrates';
    console.log('API URL for all categories: ', this.apiURL);
    this.categories = this.http.get(url);
    return this.categories;
  }



  placeOrder(orderDataObj){
    let headers = new HttpHeaders ({
      'Content-Type': 'application/json'
    });

    let orderData = this.JSON_to_URLEncoded(orderDataObj);



    return new Promise ((resolve) => {
      this.orderResp = this.http.post(this.apiURL, orderData, {headers});
      this.orderResp.subscribe((responseData) => {
        resolve(responseData);
      });
    });
  }
  JSON_to_URLEncoded(element, key?, list?) {
    var list = list || [];
    if (typeof element == "object") {
      for (var idx in element)
        this.JSON_to_URLEncoded(
          element[idx],
          key ? key + "[" + idx + "]" : idx,
          list
        );
    } else {
      list.push(key + "=" + encodeURIComponent(element));
    }
    return list.join("&");
  }

}

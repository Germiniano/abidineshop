import {DatePipe, formatDate} from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { EcommerceService} from 'src/app/ecommerce.service';
import { Router } from '@angular/router';
import { UserService } from '../user.service';
@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {

  constructor(private ec: EcommerceService,
              private userservice: UserService,
              private router: Router) { }
  myDate: any;
  user: any;
  categories: any;
  term:any;
  ngOnInit(): void {
    this.myDate = formatDate(new Date(), 'dd/MM/yyyy', 'en');
    this.ec.getAllCategories().subscribe((data) => {
      this.categories = data;
      console.log(this.categories);
    });
  }
  setTerm(event: Event){
    this.term =(<HTMLInputElement> event.target).value;
    console.log(this.term);
  }
}


import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigfaqComponent } from './configfaq.component';

describe('ConfigfaqComponent', () => {
  let component: ConfigfaqComponent;
  let fixture: ComponentFixture<ConfigfaqComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigfaqComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigfaqComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

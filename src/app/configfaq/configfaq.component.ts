import {DatePipe, formatDate} from '@angular/common';
import { Component, OnInit , EventEmitter} from '@angular/core';
import * as ClassicEditorBuild from '@ckeditor/ckeditor5-build-classic';
import { CKEditor5 } from '@ckeditor/ckeditor5-angular/ckeditor';
import { PageService } from '../page.service';
@Component({
  selector: 'app-configfaq',
  templateUrl: './configfaq.component.html',
  styleUrls: ['./configfaq.component.css']
})
export class ConfigfaqComponent implements OnInit {
  page: any;
  public ClassicEditorBuild = ClassicEditorBuild;
  public editor: CKEditor5.Editor = null;
  public readyEmitter = new EventEmitter<CKEditor5.Editor>();
  faqs: any[];
  question: any;
  answer: any;
  myDate:any;
  constructor( private pageService: PageService ) {
    this.page = {};
    this.faqs=[];
    this.pageService.getFAQData().subscribe(data=>{
      this.page=data;
    })
  }

  ngOnInit(): void {
    this.myDate = formatDate(new Date(), 'dd/MM/yyyy', 'en');
  }
  public onReady( editor: CKEditor5.Editor ) {
    this.editor = editor;
    this.readyEmitter.emit( this.editor );
  }

  setMetaTagDescription(event: Event){
    this.page.tagdescription = (event.target as HTMLInputElement).value;
    console.log(this.page);
    console.log(this.editor.getData());
  }
  setTagTitle(event: Event){
    console.log(this.page);
    this.page.tagtitle = (event.target as HTMLInputElement).value;
    console.log(this.editor.getData());
  }
  setMetaTagKeywords(event: Event){
    console.log(this.page);
    this.page.tagkeywords = (event.target as HTMLInputElement).value;
    console.log(this.editor.getData());
  }
  setQuestion(event: Event){
    console.log(this.page);
    this.page.tagkeywords = (event.target as HTMLInputElement).value;
    console.log(this.editor.getData());
  }
  setAnswer(event: Event){
    console.log(this.page);
    this.page.tagkeywords = (event.target as HTMLInputElement).value;
    console.log(this.editor.getData());
  }
  addQuestionGroup(){
    this.faqs.push({question: this.question, answer: this.answer});
  }
  savePage(){

    this.page.faqs=this.faqs;
    this.pageService.setFAQData(this.page).subscribe((data) => {
      console.log(data);
      if(data){alert("Atulizados");}
      if(Error){alert("error");}
    });
  }
}

import {DatePipe, formatDate} from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { EcommerceService} from 'src/app/ecommerce.service';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-client-rates',
  templateUrl: './client-rates.component.html',
  styleUrls: ['./client-rates.component.css']
})
export class ClientRatesComponent implements OnInit {
rates: any;
myDate:any;
  constructor(private ec: EcommerceService,
              private userservice: UserService,
              private router: Router) { }

  ngOnInit(): void {
    this.myDate = formatDate(new Date(), 'dd/MM/yyyy', 'en');
    this.ec.getAllClientRatings().subscribe((data) => {
      this.rates = data;

    });
  }

}

import { Component, OnInit } from '@angular/core';
import { EcommerceService} from 'src/app/ecommerce.service';
import {ActivatedRoute, Router} from '@angular/router';
import { UserService } from '../user.service';
import { ToastrService } from 'ngx-toastr';
import {  ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import * as _ from 'lodash';

@Component({
  selector: 'app-editproduct1',
  templateUrl: './editproduct1.component.html',
  styleUrls: ['./editproduct1.component.css']
})
export class Editproduct1Component implements OnInit {

  name: string = '';
  desc: string = '';
  shortdesc: string = '';
  imgpath: string = '';
  oldprice: any;
  actualprice: any;
  videourl: any;
  user: any;
  product: any;
  myDate:any;
  title = 'angular-image-file-upload-tutorial';
  categoriesAdded = [];

  @ViewChild('UploadFileInput', { static: false }) uploadFileInput: ElementRef;
  fileUploadForm: FormGroup;
  fileInputLabel: string;


  constructor(  private ec: EcommerceService,
                private router: Router, private toastr: ToastrService,
                private userservice: UserService,
                private http: HttpClient,
                private formBuilder: FormBuilder,
                public activatedRoute: ActivatedRoute
  ) { }



  ngOnInit(): void {

    if (this.userservice.isSigned() === false ){
      this.router.navigateByUrl('/login');
    }else{
      this.user = this.userservice.isSigned();
    }
    this.product = {};
    this.fileUploadForm = this.formBuilder.group({
      uploadedImage: ['']
    });
    this.activatedRoute.paramMap.subscribe((paramMap) => {
      this.product._id = paramMap.get('pid');
      console.log('Product ID found: ', paramMap.get('pid'));
      this.product.categories = [];
      this.categoriesAdded = [];
    });
  }
  onFileSelect(event) {
    const file = event.target.files[0];
    this.fileInputLabel = file.name;
    this.fileUploadForm.get('uploadedImage').setValue(file);
  }
  onFormSubmit() {

    if (!this.fileUploadForm.get('uploadedImage').value) {
      alert('Please fill valid details!');
      return false;
    }

    const formData = new FormData();
    formData.append('uploadedImage', this.fileUploadForm.get('uploadedImage').value);
    formData.append('agentId', '007');


    this.http
      .post<any>('http://localhost:3100/api/fileupload', formData).subscribe(response => {
      console.log(response);
      if (response.statusCode === 200) {
        // Reset the file input
        this.uploadFileInput.nativeElement.value = "";
        this.fileInputLabel = undefined;
      }
    }, er => {
      console.log(er);
      alert(er.error.error);
    });
  }




  addProductName( event: Event){
    this.product.name = (<HTMLInputElement> event.target).value;
    console.log(this.product);
  }
  addProductDesc( event: Event){
    this.product.fulldescription = (<HTMLInputElement> event.target).value;
    console.log(this.product);
  }
  addProductShortDesc(event: Event){
    this.product.shortdescription = (<HTMLInputElement> event.target).value;
    console.log(this.product);
  }
  addProductOldPrice (event: Event){
    this.product.oldprice = parseFloat((<HTMLInputElement> event.target).value);
    console.log(this.product);
  }
  addProductActualPrice (event: Event){
    this.product.price = parseFloat((<HTMLInputElement> event.target).value);
    console.log(this.product);
  }
  addProductVideoURL (event: Event){
    this.product.videourl = (<HTMLInputElement> event.target).value;
    console.log(this.product);
  }
  addProductMostSold(event: Event){
    this.product.mostSold = true;
    console.log(this.product);
  }
  addProductSecondwDisc(event: Event){
    this.product.secondwDisc = true;
    console.log(this.product);
  }
  addProductP2l3(event: Event){
    this.product.p2l3 = true;
    console.log(this.product);
  }
  addProductImgPath (event: Event){
    this.product.photourl = (<HTMLInputElement> event.target).value;
    console.log(this.product);
  }
  nextStep(){
    let productString = JSON.stringify(this.product);
    this.router.navigateByUrl('/editarprodutosform2/'+ productString);
  }

}





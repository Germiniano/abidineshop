import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Editproduct1Component } from './editproduct1.component';

describe('Editproduct1Component', () => {
  let component: Editproduct1Component;
  let fixture: ComponentFixture<Editproduct1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Editproduct1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Editproduct1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HelpcenterintComponent } from './helpcenterint.component';

describe('HelpcenterintComponent', () => {
  let component: HelpcenterintComponent;
  let fixture: ComponentFixture<HelpcenterintComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HelpcenterintComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HelpcenterintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

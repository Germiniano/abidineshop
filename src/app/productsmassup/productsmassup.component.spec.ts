import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductsmassupComponent } from './productsmassup.component';

describe('ProductsmassupComponent', () => {
  let component: ProductsmassupComponent;
  let fixture: ComponentFixture<ProductsmassupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductsmassupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsmassupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

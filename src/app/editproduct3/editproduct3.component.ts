import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../user.service';
@Component({
  selector: 'app-editproduct3',
  templateUrl: './editproduct3.component.html',
  styleUrls: ['./editproduct3.component.css']
})
export class Editproduct3Component implements OnInit {

  techSpecsTable: any;
  characterTable: any;
  user: any;
  product: any;
  myDate:any;
  constructor(private router: Router,
              public activatedRoute: ActivatedRoute,
              private userservice: UserService
   ) { }



  ngOnInit(): void {
    if (this.userservice.isSigned() === false ){
      this.router.navigateByUrl('/login');
  }else{
      this.user = this.userservice.isSigned();
  }
    this.activatedRoute.paramMap.subscribe((paramMap) => {
      this.product = JSON.parse(paramMap.get('product'));
      console.log('Product ID found: ', this.product);

    });
    this.product.ProductCharacterics =[];
    this.product.ProductSpecification =[];
  }






addProductCharacteric( event: Event,index: number){
  this.product.ProductCharacterics[index]= (<HTMLInputElement> event.target).value;
  console.log(this.product);
}
addProductSpecification( event: Event,index: number){
  this.product.ProductSpecification[index]= (<HTMLInputElement> event.target).value;
  console.log(this.product);
}


nextStep(){
  let productString = JSON.stringify(this.product);
  this.router.navigateByUrl('/editarprodutosform4/'+ productString);
}


}

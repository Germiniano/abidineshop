import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Editproduct3Component } from './editproduct3.component';

describe('Editproduct3Component', () => {
  let component: Editproduct3Component;
  let fixture: ComponentFixture<Editproduct3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Editproduct3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Editproduct3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

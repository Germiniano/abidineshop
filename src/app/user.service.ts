import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  orderResp: any;
  products: any;
    product: any;
    categories: any;
    tags: any;
    apiURL = 'http://localhost:3100/api';
    consumerKey = 'ck_63e89ba4a2bc6a5f4bdb9c984f150c5350bfb605';
    consumerSecret = 'cs_3c0fb48a388b6e8a983eaa3f43ad9acaf32d1903';
    data:any;
    user:any;

  constructor(private http: HttpClient) {   }

  signIn(user,pass){
    this.data ={ } ;
    this.data.password=pass;
    this.data.email=user;
    const url = this.apiURL + '/user/singin';
    console.log(this.data);

    this.user = this.http.post("https://stormdeveloper.com.br:3100/api/signin",
     {"email": String(user), "password": String(pass)}
    );
    console.log(this.user.user);

    return this.user;
  }

  signOut(pId){
   const url = this.apiURL + '/product/' + pId;
   console.log('API URL for single product: ', this.apiURL);
   this.product = this.http.get(url);
   return this.product;
  }

  signUp(newUser){
    let data = newUser;
    this.user = this.http.post("https://stormdeveloper.com.br:3100/api/signup", data );
   console.log(this.user.user);

   return this.user;
  }
  isSigned(){
    if(sessionStorage.getItem('token')!==null){
      console.log(" logado");
      console.log(sessionStorage.getItem('user'));
      this.user = JSON.parse(sessionStorage.getItem('user'));
      return this.user;
  }else{
    console.log("nao esta logado");
    return false;
  }
  }


  addCompany(company){
    const url = this.apiURL + '/addcompany';
    const data = this.http.post(url ,company);
    return data;
  }


}

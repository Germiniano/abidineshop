import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigprivacyComponent } from './configprivacy.component';

describe('ConfigprivacyComponent', () => {
  let component: ConfigprivacyComponent;
  let fixture: ComponentFixture<ConfigprivacyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigprivacyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigprivacyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

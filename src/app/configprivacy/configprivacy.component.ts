import {DatePipe, formatDate} from '@angular/common';
import { Component, OnInit , EventEmitter} from '@angular/core';
import * as ClassicEditorBuild from '@ckeditor/ckeditor5-build-classic';
import { CKEditor5 } from '@ckeditor/ckeditor5-angular/ckeditor';
import { PageService } from '../page.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-configprivacy',
  templateUrl: './configprivacy.component.html',
  styleUrls: ['./configprivacy.component.css']
})
export class ConfigprivacyComponent implements OnInit {

  page:any;
  myDate:any
  public ClassicEditorBuild = ClassicEditorBuild;
  public editor: CKEditor5.Editor = null;
  public readyEmitter = new EventEmitter<CKEditor5.Editor>();
  constructor( private pageService: PageService, private router: Router  ) {


    this.page = {};
  }

  ngOnInit(): void {
    this.myDate = formatDate(new Date(), 'dd/MM/yyyy', 'en');
    this.pageService.getPrivacyData().subscribe(data=>{
      this.page=data[0];
      console.log(data);
    })
  }
  public onReady( editor: CKEditor5.Editor ) {
    this.editor = editor;
    this.readyEmitter.emit( this.editor );
  }

  setMetaTagDescription(event: Event){
    this.page.tagdescription = (event.target as HTMLInputElement).value;
    console.log(this.page);
    console.log(this.editor.getData());
  }
  setTagTitle (event: Event){
    console.log(this.page);
    this.page.tagtitle = (event.target as HTMLInputElement).value;
    console.log(this.editor.getData());
  }
  setMetaTagKeywords(event: Event){
    console.log(this.page);
    this.page.tagkeywords = (event.target as HTMLInputElement).value;
    console.log(this.editor.getData());
  }
  savePage(){
    this.page.ckeditor = this.editor.getData();
    this.pageService.setPrivacyData(this.page).subscribe((data) => {
      console.log(data);
      if(data.result== "update page"){
        alert("Página atualizada com sucesso.");
      }else{
        alert("Erro ao inserir a configuração");
      }
    });
  }
}

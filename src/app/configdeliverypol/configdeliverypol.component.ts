import {DatePipe, formatDate} from '@angular/common';
import { Component, OnInit , EventEmitter} from '@angular/core';
import * as ClassicEditorBuild from '@ckeditor/ckeditor5-build-classic';
import { CKEditor5 } from '@ckeditor/ckeditor5-angular/ckeditor';
import { PageService } from '../page.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-configdeliverypol',
  templateUrl: './configdeliverypol.component.html',
  styleUrls: ['./configdeliverypol.component.css']
})
export class ConfigdeliverypolComponent implements OnInit {

  page:any;
  myDate:any;
  public ClassicEditorBuild = ClassicEditorBuild;
  public editor: CKEditor5.Editor = null;
  public readyEmitter = new EventEmitter<CKEditor5.Editor>();
  constructor( private pageService: PageService, private router: Router ) {


    this.page = {};
  }

  ngOnInit(): void {
    this.myDate = formatDate(new Date(), 'dd/MM/yyyy', 'en');
  }
  public onReady( editor: CKEditor5.Editor ) {
    this.editor = editor;
    this.readyEmitter.emit( this.editor );
  }

  setMetaTagDescription(event: Event){
    this.page.tagdescription = (<HTMLInputElement> event.target).value;
    console.log(this.page);
    console.log(this.editor.getData());
  }
  setTagTitle (event: Event){
    console.log(this.page);
    this.page.tagtitle = (<HTMLInputElement> event.target).value;
    console.log(this.editor.getData());
  }
  setMetaTagKeywords(event: Event){
    console.log(this.page);
    this.page.tagkeywords = (<HTMLInputElement> event.target).value.split(" ");
    console.log(this.editor.getData());
  }
  savePage(){
    this.page.ckeditor = this.editor.getData();
    this.pageService.setDeliveryData(this.page).subscribe((data) => {
      console.log(data);
      if(data.result== "update page"){
        alert("Página atualizada com sucesso.");
      }else{
        alert("Erro ao inserir a configuração");
      }
    });
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigdeliverypolComponent } from './configdeliverypol.component';

describe('ConfigdeliverypolComponent', () => {
  let component: ConfigdeliverypolComponent;
  let fixture: ComponentFixture<ConfigdeliverypolComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigdeliverypolComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigdeliverypolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

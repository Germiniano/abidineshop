import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MsgsService {


  page: any;
  apiURL = 'http://localhost:3100/api/client/'; // 'https://stormdeveloper.com.br:3100/api/pages';


  constructor(private http: HttpClient) {   }

  getClientMsgs( faq){
    const url = this.apiURL + '/msg';
    console.log('API URL for all store products: ', this.apiURL);
    this.page = this.http.get(url);
    return  this.page;
  }



}

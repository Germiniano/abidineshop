import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  username :any;
  password : any;
  token:any;
  user:any;
  constructor(private userService : UserService, private router :Router) { }

  ngOnInit(): void {

  }


  setUser(event: Event){
    this.username=(<HTMLInputElement> event.target).value;
    console.log(this.username);
  }
  setPass(event: Event){
    this.password=(<HTMLInputElement> event.target).value;
    console.log(this.password);
  }
  login(){        
    if(this.validateForm()!=''){ alert(this.validateForm())};
    this.userService.signIn(this.username, this.password).subscribe((data) => {
      if(data.token){
        this.user=data;
        console.log(this.user);
        sessionStorage.setItem('token', this.user.token);
        sessionStorage.setItem('user', JSON.stringify(this.user.user));
        this.router.navigateByUrl('/');

      }else{
        alert("e-mail e/ou senha invalidos");
        console.log("e-mail e/ou senha invalidos");
      }

    });
  
   
  }
  validateForm(){
    if(!this.username) return 'Digite o Login';
    if(!this.password) return 'Digite a Senha';
   
     return''; 
    
    }

}

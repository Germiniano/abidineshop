import { Component, OnInit } from '@angular/core';
import {DatePipe, formatDate} from '@angular/common';

@Component({
  selector: 'app-register4',
  templateUrl: './register4.component.html',
  styleUrls: ['./register4.component.css']
})
export class Register4Component implements OnInit {
  myDate:any;

  constructor() { }

  ngOnInit(): void {  
      this.myDate = formatDate(new Date(), 'dd/MM/yyyy', 'en');

  }

}

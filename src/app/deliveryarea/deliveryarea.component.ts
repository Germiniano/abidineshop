import { Component, OnInit } from '@angular/core';
import {DatePipe, formatDate} from '@angular/common';

@Component({
  selector: 'app-deliveryarea',
  templateUrl: './deliveryarea.component.html',
  styleUrls: ['./deliveryarea.component.css']
})
export class DeliveryareaComponent implements OnInit {
  myDate: any;
  constructor() { }

  ngOnInit(): void {
    this.myDate = formatDate(new Date(), 'dd/MM/yyyy', 'en');

  }

}

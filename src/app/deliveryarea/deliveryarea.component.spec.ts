import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliveryareaComponent } from './deliveryarea.component';

describe('DeliveryareaComponent', () => {
  let component: DeliveryareaComponent;
  let fixture: ComponentFixture<DeliveryareaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliveryareaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliveryareaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfighelpcenterComponent } from './confighelpcenter.component';

describe('ConfighelpcenterComponent', () => {
  let component: ConfighelpcenterComponent;
  let fixture: ComponentFixture<ConfighelpcenterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfighelpcenterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfighelpcenterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

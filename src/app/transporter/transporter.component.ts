import {DatePipe, formatDate} from '@angular/common';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-transporter',
  templateUrl: './transporter.component.html',
  styleUrls: ['./transporter.component.css']
})



export class TransporterComponent implements OnInit {
  myDate:any;
  faixas:any[];
  faixa={
    region:'',
    cepInit:'',
    cepEnd:'',
    dl:'',
    value:''
  };
  region: any;
  cepInit: any;
  cepEnd: any;
  value: any;
  dl: any;
  constructor() { }

  ngOnInit(): void {
    this.myDate = formatDate(new Date(), 'dd/MM/yyyy', 'en');

    this.faixas=[];
  }
  setRegion(event){
    this.region= (<HTMLInputElement> event.target).value;
    console.log(this.region);
  }
  setCEPInit(event){
    this.cepInit=(<HTMLInputElement> event.target).value;
  }
  setCEPEnd(event){
    this.dl= (<HTMLInputElement> event.target).value;
  }
  setDeadline(event){
    this.dl= (<HTMLInputElement> event.target).value;
  }
  setValue(event){
    this.value= (<HTMLInputElement> event.target).value;
  }
  addFaixa(){
    alert("foi")
    let  faixa={
      region:this.region,
      cepInit:this.cepInit,
      cepEnd: this.cepEnd,
      dl:this.dl,
      value:''
    };
    this.faixas.push(faixa);

  }


}

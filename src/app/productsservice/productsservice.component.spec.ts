import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductsserviceComponent } from './productsservice.component';

describe('ProductsserviceComponent', () => {
  let component: ProductsserviceComponent;
  let fixture: ComponentFixture<ProductsserviceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductsserviceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsserviceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

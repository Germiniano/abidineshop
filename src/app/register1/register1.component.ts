import { Component, OnInit } from '@angular/core';
import {DatePipe, formatDate} from '@angular/common';

@Component({
  selector: 'app-register1',
  templateUrl: './register1.component.html',
  styleUrls: ['./register1.component.css']
})
export class Register1Component implements OnInit {
  myDate:any;
  login:any;
  password:any;
  passwordConfirm:any;
  email:any;
  user:any;

  constructor() { }

  ngOnInit(): void {
    this.user={};
    this.myDate = formatDate(new Date(), 'dd/MM/yyyy', 'en');

  }
  create(){
    this.user.login =this.login;
    this.user.email=this.email;
    this.user.password =this.password;

   if( this.user.password  ===  this.passwordConfirm){

   }

  }

}

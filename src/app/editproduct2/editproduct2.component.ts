import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../user.service';
import { EcommerceService } from '../ecommerce.service';
@Component({
  selector: 'app-editproduct2',
  templateUrl: './editproduct2.component.html',
  styleUrls: ['./editproduct2.component.css']
})
export class Editproduct2Component implements OnInit {


  myDate:any;
  product: any;
  categories: any;
  categoriesAdded: any;
  user: any;
  constructor(private router: Router,
              public activatedRoute: ActivatedRoute,
              private ec: EcommerceService,
              private userService: UserService
   ) { }



  ngOnInit(): void {

    if (this.userService.isSigned() === false ){
        this.router.navigateByUrl('/login');
    }else{
        this.user = this.userService.isSigned();
    }
    this.activatedRoute.paramMap.subscribe((paramMap) => {
      this.product = JSON.parse(paramMap.get('product'));
      console.log('Product ID found: ', this.product);
      this.product.categories = [];
      this.categoriesAdded = [];
    });
    this.ec.getAllCategories().subscribe((data) => {
      this.categories = data;
      console.log(this.categories);
    });
  }



addCategory(id,name){
  this.product.categories.push( parseInt(id) );
  console.log(this.product.categories);
}
removeCategory(e: Event ){

}

nextStep(){
  console.log(this.product);
  let productString = JSON.stringify(this.product);
  this.router.navigateByUrl('/editarprodutosform3/'+ productString);
}


}

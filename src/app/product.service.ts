import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
product = {
sku:   '',
name : '',
fulldescription  : '',
shortdescription : '',
videourl : '',
isvariation : false,
isactive :true,
attributes : [],
price : 0,
oldprice : 0,
promoprice : 0,
quantity : 0,
sold : 0,
photourl : [],
shipping :false,
stockmanagement : false,
fixedamount : false,
gtin : '' ,
twoperone : false,
mostsold : false,
threepertwo : false,
sedcondwdisc : false,
categories : [],
techspec : [],
techchar : [],
variations : [],
productpagetitle : '',
productpagedesc : '',
productpagekeywords : [],
weight : 0,
width : 0,
height : 0,
length : 0
}

  constructor() { }


get(){
  return this.product;
}
set( product ){
  this.product = product;
}

}

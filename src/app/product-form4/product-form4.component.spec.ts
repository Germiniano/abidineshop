import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductForm4Component } from './product-form4.component';

describe('ProductForm4Component', () => {
  let component: ProductForm4Component;
  let fixture: ComponentFixture<ProductForm4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductForm4Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductForm4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

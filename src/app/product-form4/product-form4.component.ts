import {DatePipe, formatDate} from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { EcommerceService} from 'src/app/ecommerce.service';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../user.service';
import { ProductService } from '../product.service';
@Component({
  selector: 'app-product-form4',
  templateUrl: './product-form4.component.html',
  styleUrls: ['./product-form4.component.css']
})
export class ProductForm4Component implements OnInit {
  variations: any[];
  user: any;
  product: any;
  myDate:any;
  constructor(private router: Router,
              public activatedRoute: ActivatedRoute,
              private userservice: UserService,
              private ps: ProductService,
              private ec: EcommerceService
) { }



ngOnInit(): void {
  this.myDate = formatDate(new Date(), 'dd/MM/yyyy', 'en');
  if (this.userservice.isSigned() === false ){
    this.router.navigateByUrl('/login');
}else{
    this.user = this.userservice.isSigned();
}
  this.product = this.ps.get();
  this.product.variations = [
    {name: 'vestido vermelho',
    price: 20,
    attributes: [
            {
                name: 'vermelho',
                type: 'color'
            },
                    {
                name: 'm',
                type: 'size'
            }
        ]

    }
];
  console.log(this.product);
}

addProductNCM(event: Event){
  this.product.ncm = (event.target as HTMLInputElement).value;
}
addProductGtin(event: Event){
  this.product.gtin = (event.target as HTMLInputElement).value;
}
addVariation(){

}

  nextStep(){
    const productString = JSON.stringify(this.product);


    this.ec.updateProduct(this.product)
       .subscribe(data => {
          let product ;
          product  =data;
          this.ps.set(product.data);
          this.router.navigateByUrl('/produtosform5');
    });
  }


}

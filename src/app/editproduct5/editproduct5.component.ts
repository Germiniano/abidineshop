import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {EcommerceService} from "../ecommerce.service";
import {UserService} from "../user.service";
import {FormGroup} from "@angular/forms";

@Component({
  selector: 'app-editproduct5',
  templateUrl: './editproduct5.component.html',
  styleUrls: ['./editproduct5.component.css']
})
export class Editproduct5Component implements OnInit {

  title = 'angular-image-file-upload-tutorial';

  @ViewChild('UploadFileInput', { static: false }) uploadFileInput: ElementRef;
  fileUploadForm: FormGroup;
  fileInputLabel: string;


  pageTitle = '';
  pageDesc = '';
  pageKeywords: any;
  product: any;
  user: any;
  myDate:any;
  constructor(private activatedRoute: ActivatedRoute,
              private ec: EcommerceService,
              private userservice: UserService,
              private router: Router
  ) { }



  ngOnInit(): void {
    if (this.userservice.isSigned() === false ){
      this.router.navigateByUrl('/login');
    }else{
      this.user = this.userservice.isSigned();
    }
    this.activatedRoute.paramMap.subscribe((paramMap) => {
      this.product = JSON.parse(paramMap.get('product'));
      console.log('Product ID found: ', this.product);

    });

  }



  addPageTitle( event: Event){
    this.product.productpagetitle= (<HTMLInputElement> event.target).value;
    console.log(this.product);
  }

  addPageDesc( event: Event){
    this.product.productpagedesc= (<HTMLInputElement> event.target).value;
    console.log(this.product);
  }

  addPageKeywords(event: Event){
    this.product.productpagekeywords = (<HTMLInputElement> event.target).value;
    console.log(this.product);
  }

  editProduct(){
    this.product.quantity = this.product.price;
    this.ec.updateProduct(this.product).subscribe(data => {

      console.log(data);
    });

    console.log(this.product);
  }



}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Editproduct5Component } from './editproduct5.component';

describe('Editproduct5Component', () => {
  let component: Editproduct5Component;
  let fixture: ComponentFixture<Editproduct5Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Editproduct5Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Editproduct5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import {DatePipe, formatDate} from '@angular/common';
import { UserService } from '../user.service';

@Component({
  selector: 'app-registercompany',
  templateUrl: './registercompany.component.html',
  styleUrls: ['./registercompany.component.css']
})
export class RegistercompanyComponent implements OnInit {
  myDate:any;
  logo:any;
  bairro:any;


  company:any;
  constructor( private userSerice: UserService) { }

  ngOnInit(): void {

    this.myDate = formatDate(new Date(), 'dd/MM/yyyy', 'en');

  }
  create(){
    this.company.bairro= this.bairro;
    this.userSerice.addCompany(this.company).subscribe( resp =>{
      console.log(resp);

    });

  }

}

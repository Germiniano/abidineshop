import { Component, OnInit } from '@angular/core';
import {DatePipe, formatDate} from '@angular/common';

@Component({
  selector: 'app-register2',
  templateUrl: './register2.component.html',
  styleUrls: ['./register2.component.css']
})
export class Register2Component implements OnInit {
  myDate:any;

  constructor() { }

  ngOnInit(): void {
    this.myDate = formatDate(new Date(), 'dd/MM/yyyy', 'en');

  }

}

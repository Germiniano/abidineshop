import {DatePipe, formatDate} from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { EcommerceService} from 'src/app/ecommerce.service';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../user.service';
import { ProductService } from '../product.service';
@Component({
  selector: 'app-product-form3',
  templateUrl: './product-form3.component.html',
  styleUrls: ['./product-form3.component.css']
})
export class ProductForm3Component implements OnInit {


  techSpecsTable: any;
  characterTable: any;
  user: any;
  product: any;
  myDate:any;
  constructor(private router: Router,
              public activatedRoute: ActivatedRoute,
              private userservice: UserService,
              private ps: ProductService,
              private ec: EcommerceService
   ) { }



  ngOnInit(): void {
    this.myDate = formatDate(new Date(), 'dd/MM/yyyy', 'en');
    if (this.userservice.isSigned() === false ){
      this.router.navigateByUrl('/login');
  }else{
      this.user = this.userservice.isSigned();
  }
  this.product=this.ps.get();
  console.log(this.product);
    this.product.ProductCharacterics =[];
    this.product.ProductSpecification =[];
  }






addProductCharacteric( event: Event,index: number){
  this.product.ProductCharacterics[index]= (<HTMLInputElement> event.target).value;
  console.log(this.product);
}
addProductSpecification( event: Event,index: number){
  this.product.ProductSpecification[index]= (<HTMLInputElement> event.target).value;
  console.log(this.product);
}


nextStep(){
  console.log(this.product);
  let productString = JSON.stringify(this.product);
  this.ps.set(this.product);
  this.product.quantity = this.product.price;
    this.ec.updateProduct(this.product)
      .subscribe(data=>{
        let product ;
        product  =data;
        this.ps.set(product.data);
        this.router.navigateByUrl('/produtosform4');
      });
}


}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductForm3Component } from './product-form3.component';

describe('ProductForm3Component', () => {
  let component: ProductForm3Component;
  let fixture: ComponentFixture<ProductForm3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductForm3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductForm3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

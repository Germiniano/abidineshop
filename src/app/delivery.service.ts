import { Injectable } from '@angular/core';


import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class DeliveryService {

method ={
  name:"sedex",
  active:true,
  ceporigem:0,
  addtime: 0,
  extrafreightvalue: 0,
  addextrafreightvalue: true,
  minlvalue:0,
  token:" "

}


  page: any;
  apiURL = 'http://localhost:3100/api/delivery'; // 'https://stormdeveloper.com.br:3100/api/pages';


  constructor(private http: HttpClient) {   }

  getDelivery( method){
    const url = this.apiURL +'/'+ method.name;

    this.page = this.http.get(url);
    return  this.page;
  }
  setDelivery(method){

    console.log(method);
    this.page = this.http.put(this.apiURL, method);
    return this.page;
  }


}

import { Component, OnInit } from '@angular/core';
import { PageService } from '../page.service';

@Component({
  selector: 'app-aboutus',
  templateUrl: './aboutus.component.html',
  styleUrls: ['./aboutus.component.css']
})
export class AboutusComponent implements OnInit {

  constructor(private ps:PageService) { }

  ngOnInit(): void {

  }
  update(){
    this.ps.setAboutUsData("").subscribe(data=>{})
  }

}

import {DatePipe, formatDate} from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { EcommerceService} from 'src/app/ecommerce.service';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  constructor(private ec: EcommerceService,
              private userservice: UserService,
              private router: Router) { }
  products: any;
  user:any;
  term: any;
  myDate:any;
  ngOnInit(): void {
    this.myDate = formatDate(new Date(), 'dd/MM/yyyy', 'en');
    if (this.userservice.isSigned() === false ){
      this.router.navigateByUrl('/login');
  }else{
      this.user = this.userservice.isSigned();
  }
    this.ec.getAllStoreProducts().subscribe((data) => {
      this.products = data;
      console.log(this.products);
    });
  }
setTerm(event: Event){
  this.term =(<HTMLInputElement> event.target).value;
}
}

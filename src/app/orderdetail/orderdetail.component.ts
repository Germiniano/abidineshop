import { Component, OnInit } from '@angular/core';
import {DatePipe, formatDate} from '@angular/common';

@Component({
  selector: 'app-orderdetail',
  templateUrl: './orderdetail.component.html',
  styleUrls: ['./orderdetail.component.css']
})
export class OrderdetailComponent implements OnInit {

  myDate:any;

  constructor() { }

  ngOnInit(): void {
    this.myDate = formatDate(new Date(), 'dd/MM/yyyy', 'en');

  }

}
